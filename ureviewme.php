<?php
/**
 * Plugin Name: uReview.me
 * Plugin URI: https://get.ureview.me/
 * Description: Reputation Matters - Generate positive reviews & attract more customers.
 * Version: 1.0.2
 * Author: uReview.me
 * Author URI: https://get.ureview.me/
 * Text Domain: ureview-me
 */
 
 /* 
	
	v.1.0.2

		- Fixed some styling issues
	
	v.1.0.1

		- Fixed some styling issues
	
	v.1.0

		- Added option to choose colors for the button
		- Added option to disable aggeregrate ratings
*/ 

/* Pusing plugin Update */
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://bitbucket.org/shabbyonline/ureview.me-reviews-plugin',
    __FILE__,
    'ureviewme-reviews-plugin'
);

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');

/* Pusing plugin Update */
 
//Constants
define("PLUGIN_DIR_PATH",plugin_dir_path(__FILE__)); // We used this path to access our plugin files mainly PHP files.
define("PLUGIN_URL",plugins_url()); // We used this path to access our plugin assets mainly images,css and js files.
define ("PLUGIN_VERSION", '1.0');
define ("RID", '1.0');
 
// Adding Custom menu to Admin
function add_my_custom_menu() {
	
   add_menu_page( // WP default function used to create menu by simply passing the parameters as below.
		__( 'Ureview Me', 'textdomain' ), // Page Title
		'uReview.me', // Menu Title
		'manage_options', // Capability Admin Level
		'ureview-me', // Page Slug / Parent Menu slug
		'add_new_funciton', //callable Function
		'dashicons-star-filled', // Plugin Icon
		78 // Menu Position
   );
}
add_action('admin_menu','add_my_custom_menu');

//This callable Function will execute when user will click the First Sub menu
function add_new_funciton(){
	include_once PLUGIN_DIR_PATH.'/views/ureview-settings.php';
}

function custom_plugin_assets(){
	
	wp_enqueue_style(
		'cpl_style', // unique name for css file
		PLUGIN_URL.'/ureview-me/assets/css/style.css', //css file path
		'', // Dependent on other file
		PLUGIN_VERSION // Plugin version nunmber
	);
	
	wp_enqueue_script(
		'cpl_script', // unique name for JS file
		PLUGIN_URL.'/ureview-me/assets/js/custom.js', //JS file path
		'', // Dependent on other file
		PLUGIN_VERSION, // Plugin version nunmber
		true
	);
	
	// Get Button and Circle Colors value from DB and pass the values to inline CSS below
	global $wpdb;
	// Fetching alues from Db
    $appTable = "wp_ureview_me";
    $query = $wpdb->prepare("SELECT * FROM $appTable", RID);
    $applications = $wpdb->get_results($query);
	// Loop to get data
    foreach ( $applications as $application ) {
        $application->buttoncolor;
		$application->circlecolor;
    }

	$ureview_btn_color = $application->buttoncolor;
	$ureview_circle_color = $application->circlecolor;
	
	// Passing the Color values in CSS
	$custom_css = "
	  .reviewstream-rating .ureview-toggle-button {
			  background-color: ". $ureview_btn_color .";
	  }
	  .reviewstream-rating .ureview-toggle-button span {
			  background-color: ". $ureview_circle_color .";
	  }
	  ";
	wp_add_inline_style( 'cpl_style', $custom_css );

}
add_action('init','custom_plugin_assets');

// Thrive Add Ureview code on Activation
function thrive_ureviewme_widget() {
	
	// Get Button and Circle Colors value from DB and pass the values to inline CSS below
	global $wpdb;
	// Fetching alues from Db
    $appTable = "wp_ureview_me";
    $query = $wpdb->prepare("SELECT * FROM $appTable", RID);
    $applications = $wpdb->get_results($query);
	// Loop to get data
    foreach ( $applications as $application ) {
        
    }

	$ureview_shortname = $application->shortname;
	$ureview_rating = $application->rating;
	
	if ( !empty($ureview_shortname) ) {
?>
	
    <div class="ureviews-widget">
	
    <div class="reviewstream-rating"><a href="#/" class="ureview-toggle-button"><span></span></a></div>
    
    <div class="ureview-popup">
    	
        <?php if( $ureview_rating == 'yes' ){ ?>
        
        <div class="reviewmgr-header no-rating">
        	<a href="#/" class="ureview-close-btn">×</a>
            <h5>What Our Clients Are Saying</h5>
        </div>
        
        <?php } else { ?>
        
        <div class="reviewmgr-header aggregrate-rating-header">
        	<a href="#/" class="ureview-close-btn">×</a>
            <div class="reviewmgr-stream" data-show-aggregate-rating="true" data-show-reviews="false" data-include-empty="false" data-review-limit="5" data-url="https://www.ureview.me/<?php echo $ureview_shortname ?>/"></div>
        </div>
        
        <?php } ?>
        
        <div class="reviewmgr-stream reviewmgr-content" data-review-limit="10" data-include-empty="false" data-url="https://www.ureview.me/<?php echo $ureview_shortname ?>/"></div>
        
        <div class="reviewmgr-writereview">
        	<a class="reviewmgr-button" href="https://www.ureview.me/<?php echo $ureview_shortname ?>/" data-replace="false">
            	<img src="<?php echo PLUGIN_URL.'/ureview-me/assets/images/write-a-review-btn.png' ?>" />
            </a>
        </div>
		
        <div class="reviewmgr-footer">
        <a href="https://rizereviews.com/" target="_blank">
        	<img src="<?php echo PLUGIN_URL.'/ureview-me/assets/images/poweredby-rize.png' ?>" alt="powered by rize reviews">
        </a>
        </div>
        
    </div>
    
</div>
    	
<?php
	}
}
add_action('wp_footer', 'thrive_ureviewme_widget');


// Table generating code
// Count funciton check if the new table we are creating already exists in db, if not then it will create the custom table in SQL DB.
// get_var function is used to Executes a SQL query and returns the value from the SQL result. 
// dbDelta function is useful for creating new tables and updating existing tables to a new structure.
// register_activation_hook - Runs when a plugin is activated.
function custom_plugin_tables(){
	
	global $wpdb;
	require_once(ABSPATH. 'wp-admin/includes/upgrade.php');
	
	if ( count( $wpdb->get_var('SHOW TABLES LIKE "wp_ureview_me"') ) == 0 ) {
		
		// Sql Query to Create table
		$sql_query_to_create_table = 'CREATE TABLE `wp_ureview_me` (
		 `id` int(11) NOT NULL AUTO_INCREMENT,
		 `shortname` varchar(150) DEFAULT NULL,
		 `rating` varchar(150) DEFAULT NULL,
		 `buttoncolor` varchar(150) DEFAULT NULL,
		 `circlecolor` varchar(150) DEFAULT NULL,
		 `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		 PRIMARY KEY (`id`)
		) ENGINE=MyISAM DEFAULT CHARSET=latin1'; 
		
		//dbDelta function is useful for creating new tables and updating existing tables to a new structure.
		dbDelta($sql_query_to_create_table);
		
	}
	
}
register_activation_hook(__FILE__,'custom_plugin_tables');

// Drop table when uninstall the plugin
// query function Perform a MySQL database query, using current database connection.
// $wpdb is a global object provided by WP to communicate with mysql
// register_deactivation_hook - Set the uninstallation hook for a plugin
function delete_table(){
	
	global $wpdb;
	$wpdb->query('Drop table If Exists wp_ureview_me');
	
}
register_uninstall_hook(__FILE__,'delete_table');