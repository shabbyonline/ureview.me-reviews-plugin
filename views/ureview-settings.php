<div style="text-align:center;">
<br/>
<img src="<?php echo PLUGIN_URL.'/ureview-me/assets/images/logo.png' ?>" />
	
<br/><h1>Reputation Matters</h1>

<h4><a href="https://get.ureview.me/" target="_blank">uReview.me</a> is a done-for-you customer review tool and service. Get more positive reviews online with minimal effort where it matters most: in front of your potential customers.</h4><br/>

</div>

<h2>uReview.me Settings</h2>

<img style="float:right; margin-top:-70px;" src="<?php echo PLUGIN_URL.'/ureview-me/assets/images/Requesting-Online-Reviews-1.png' ?>" />

<?php

    if (!empty($_POST)) {
        global $wpdb;
        $table = "wp_ureview_me";
        $data = array(
			'id' => 1,
			'shortname' => $_POST['ur_path'],
            'rating'    => $_POST['ur_hide_reviews'],
			'buttoncolor'    => $_POST['ur_buttoncolor'],
			'circlecolor'    => $_POST['ur_circlecolor']
        );
        $format = array(
            '%d',
			'%s',
            '%s',
			'%s',
			'%s'
        );
		
		$success=$wpdb->replace( $table, $data, $format );
		
		if($success){
			
		//getting the values from DB and passing to fields so they auto populate if the values already exists in the DB
		global $wpdb;
        $ureviewtable = "wp_ureview_me";
		$dataquery = $wpdb->prepare("SELECT * FROM $ureviewtable", RID);
    	$ureviewapplications = $wpdb->get_results($dataquery);
		
		//print_r($ureviewapplications);
		
		foreach ( $ureviewapplications as $ureviewapplication ) {

		}
		
		$ureview_shortname = $ureviewapplication->shortname;
		$ureview_rating = $ureviewapplication->rating;
		$ureview_button = $ureviewapplication->buttoncolor;
		$ureview_circle = $ureviewapplication->circlecolor;
			
		?>
        	
            <form method="post" class="ureview-settings-form">
            
            <div class="field-col">
            	<label>uReview.me path or shortname</label>
            	<input required="required" type="text" name="ur_path" id="ur_path" value="<?php echo $ureview_shortname ?>" /><br />
                <small>(e.g. for ureview.me/yourname use <strong><em>yourname</em></strong>)</small>
            </div>
            
            <div class="field-col">
            	<label>Hide Aggregate rating</label>
            	<input style="margin-top:5px;" type="text" name="ur_hide_reviews" id="ur_hide_reviews" value="<?php echo $ureview_rating ?>">
                <small>yes/no</small>
            </div>
            
            <div class="field-col">
            	<label>Chooose a color for Button</label>
            	<input name="ur_buttoncolor" id="ur_buttoncolor" type="color" value="<?php echo $ureview_button ?>" />
            </div>
            
            <div class="field-col">
            	<label>Chooose a color for Circle</label>
            	<input name="ur_circlecolor" id="ur_circlecolor" type="color" value="<?php echo $ureview_circle ?>" />
            </div>
            
            <input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes">
        	
            </form>
        	<br />
            Your settings have been saved.
            
        <?php 
        }
    } else {
		
		//getting the values from DB and passing to fields so they auto populate if the values already exists in the DB
		global $wpdb;
        $ureviewtable = "wp_ureview_me";
		$dataquery = $wpdb->prepare("SELECT * FROM $ureviewtable", RID);
    	$ureviewapplications = $wpdb->get_results($dataquery);
		
		//print_r($ureviewapplications);
		
		foreach ( $ureviewapplications as $ureviewapplication ) {

		}
		
		$ureview_shortname = $ureviewapplication->shortname;
		$ureview_rating = $ureviewapplication->rating;
		$ureview_button = $ureviewapplication->buttoncolor;
		$ureview_circle = $ureviewapplication->circlecolor;
		
		
        ?>
        
        <form method="post" class="ureview-settings-form">
        	
            <div class="field-col">
            	<label>uReview.me path or shortname</label>
            	<input type="text" name="ur_path" id="ur_path" value="<?php echo $ureview_shortname ?>" /><br />
                <small>(e.g. for ureview.me/yourname use <strong><em>yourname</em></strong>)</small>
            </div>
            
            <div class="field-col">
            	<label>Hide aggregate rating</label>
            	<input style="margin-top:5px;" type="text" name="ur_hide_reviews" id="ur_hide_reviews" value="<?php echo $ureview_rating ?>">
                <small>yes/no</small>
            </div>
            
            <div class="field-col">
            	<label>Chooose a color for button</label>
            	<input required="required" name="ur_buttoncolor" id="ur_buttoncolor" type="color" value="<?php echo $ureview_button ?>" />
            </div>
            
            <div class="field-col">
            	<label>Chooose a color for star background</label>
            	<input required="required" name="ur_circlecolor" id="ur_circlecolor" type="color" value="<?php echo $ureview_circle ?>" />
            </div>
            
            <input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes">
        
        </form>
        
        <?php 
    } 
	

    //getColors();

?>

<style>
#ur_buttoncolor, 
#ur_circlecolor {
    width: 26px;
    height: 24px;
    padding: 0px 2px !important;
    box-shadow: none;
}
.ureview-settings-form {
	width: 400px;
    padding: 20px;
    background: #fff;
    border: 1px solid #c4ceb3;
}
.ureview-settings-form .field-col {
	display: block;
    margin-bottom: 15px;
}
.ureview-settings-form .field-col label {
    margin-right: 7px;
    vertical-align: text-bottom;
}
.ureview-settings-form .field-col small {
</style>